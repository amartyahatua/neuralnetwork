P = [2 1 -2 -1;2 -2 2 1];
t = [0 1 0 1];
net = newlin( [-2 2; -2 2],1);
net.trainParam.goal= 0.001;
[net, tr] = train(net,P,t);
weights = net.iw{1,1};
bias = net.b(1);

A = sim(net, p);
err = sum(t - sim(net,P))